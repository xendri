<?php
	$this->config = array(
		'host'              => 'localhost',
		'user'              => 'root',
		'password'          => '',
		'database'          => 'db_chedim_1',
                'cache_types'       => false,
                // since d.48
                'names'             => 'UTF-8'
	);
?>
