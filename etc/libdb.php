<?
$this->config = array(
	'library_directories'=> array(
		'lib/core/',				// Xendri Core elements
		'usr/response/',			// response modules
		'lib/ajax/',				// Ajax libraries
		'lib/csda/',				// DAL
		'usr/pe/', 				// PageElements
                'lib/type/',                            // Types
                'io/',
                'io/wrappers/',                         // file protocols wrappers
                'lib/interfaces/',                      // extensions interfaces
                'lib/ext/',                             // extensions
                'usr/furl/'                             // FURL parsers
	),
	'camel_libdir'	=> 'lib/',
	'camel_usrdir'	=> 'usr/'
);?>