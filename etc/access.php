<?
$this->config = array(
	'control_images_referrer' => true,
	'bad_referrer_image' => 'bad_referrer.png',
	'cache_time'                =>      0,
	'gzip_level'                =>      9,
	'cache_drop_interval'       =>      1,
	'cache_last_drop'           =>      0,
        'cache_dir'                 =>      './etc/cache/'
);

?>