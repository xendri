<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class FurlNews implements FurlParser {
    public static function parse(string $furl) {
        $url = substr($furl, 1);
        $url = explode('/', $url);


        $ret = array(
            'xdyn'          => 'news',
            'query_type'    => FURLDefault::ext($furl)
        );


        if (Guardian::is_int($url[1])) {
            $ret['page'] = 'year';
            $ret['year'] = $url['1'];
            if (Guardian::is_int($url[2])) {
                $ret['page'] = 'month';
                $ret['month'] = $url[2];
                if (Guardian::is_int($url[3])) {
                    $ret['page'] = 'day';
                    $ret['day'] = $url[3];
                    if (key_exists(4, $url)) {
                        $ret['page'] = 'novelty';
                        $ret['novelty'] = FSTools::filename($url[4]);
                    }
                }
            }
        } else {
             $fun = array_shift(explode('.', $url[1]));
             if (method_exists(FURLNews, $fun)) self::$fun($url, $ret);
             else {
                 $ret = array(
                     'page'=>$fun,
                     'xdyn'=>'news'
                 );
             }
        }

        return $ret;
    }

    private static function add($url, &$ret) {
        $ret['page'] = 'add';
        $ret['xdyn'] = 'news';
    }
}

?>
