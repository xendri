<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
*/

class FurlFile implements FurlParser {
    public static function parse(string $furl) {
        $furl = urldecode($furl);
        $file = substr($furl, 6);
        $fext = FSTools::fileext($file);
        $fname = substr($file, 0, strlen($fext)*-1-1);
        $return = array(
            'query_type' => $fext,
            'page'=>$fname
        );
        return $return;
    }
}

?>
