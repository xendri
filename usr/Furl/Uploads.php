<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
*/

class FurlUploads implements FurlParser {

    public static function parse(string $furl) {
        $ret = array();
        if (preg_match('%/uploads/([^/]+)/%', $furl, $match)) {
            $fid = $match[1];
            if (!Guardian::is_int($fid)) {
                if (!method_exists(FURL_Uploads, $match[1])) Log::fatal('Not integer id given!');
                self::$match[1]($furl);
            }
            try {
                $file = DataSource::selectRecord('self, $file.', 'file.id = '.$fid);
            } catch( Exception $e) {
                $file = DataSource::selectRecord('cercea, $file.', 'file.id = '.$fid);
            }
            $fname = 'etc/uploads/'.$fid;
            if (!file_exists($fname)) {
                DataSource::deleteRecord($file);
            }
            header('Content-Type: '.$file->type.'; encoding=windows-1251');
            header('Content-Disposition: attachment; filename='.FSTools::download_name_encode($file->name));
            header('Content-Length: '.filesize($fname));
            die(file_get_contents($fname));
        }

        die($furl);

        return $ret;
    }

    static function drop($furl) {
        preg_match('%/uploads/drop/(\d+)/%', $furl, $ms);
        $fid = $ms[1];

        if (!Guardian::is_int($fid)) Log::fatal('Not integer id given!');
        $file = DataSource::selectRecord('self, $file.', "file.id=$fid");
        $r = new TextReturn('etc/uploads/'.$file->id().' deleted');
        unlink('etc/uploads/'.$file->id());
        DataSource::deleteRecord($file);
        die($r->toXML()->asXML());
    }
}


?>
