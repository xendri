<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class FurlDefault implements FurlParser {

    public static function parse(string $furl) {
        $ret = array();
        if (preg_match('%(/[^/?]*/)?([^?]+/)?([^?/]+)\.([^?]+)(\?.*)?%', $furl, $match)) {
            if($match[1]) $ret['xdyn'] = substr($match[1], 1, -1);
            else $ret['xdyn'] = 'main';

            if($match[3]) $ret['page'] = $match[2].$match[3];
            else $ret['page'] = 'index';

            if($match[4]) $ret['query_type'] = $match[4];
            else $ret['query_type'] = 'html';
        }


        return $ret;
    }

}

?>
