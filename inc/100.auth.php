<?php	
$cfg = X::cfg('auth');
try {
    xAuth::pass(X::param('user'), X::param('password'));
    xAuth::continue_session();

    $hash = md5($_SERVER['REMOTE_ADDR'].'='.@json_encode(get_browser())).'='.$_SERVER['HTTP_USER_AGENT'];
    xAuth::guest($hash);
    
} catch (Exception $e) { 
    echo $e->getFile().'('.$e->getLine().'):'.$e->getMessage();
}

?>