<?php
class XSLTResponse implements Response
{
	function __construct()
	{
		header('Content-type: text/xslt');
	}

	function out()
	{
		$file = Request::param('file');
		if (strstr($file, '.')) throw new Exception('XSLT filename can`t contain dots!');
		if (file_exists('usr/xslt/setup/'.$file.'.xslt'))
		{
			echo file_get_contents('usr/xslt/setup/'.$file.'.xslt');
		} else throw new XMLException(0, 'Error: usr/xslt/setup/'.$file.'.xslt not found...');
	}
}
?>