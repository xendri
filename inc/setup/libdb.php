<?
	$this->config = Array(
		'HTTPResponse'	=>	'inc/setup/HTTPResponse.php',
		'Log'			=>	'lib/Log.php',
		'QConst'		=>	'lib/QConst.php',
		'Request'		=>	'lib/Request.php',
		'Response'		=>	'lib/Response.php',
		'SQLImport'		=>	'lib/SQLImport.php',
		'XMLException'	=>	'lib/XMLException.php',
		'XSLTResponse'	=>	'inc/setup/XSLTResponse.php'
	);
?>