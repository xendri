<?php
// processing query
X::notice ( 'processing query: ' . $_SERVER ['REQUEST_URI'] );
$rtype = xRequest::type ();
if (strtolower ( $rtype ) == 'htm')
    $rtype = 'html';
$resp = 'xRenderer'.ucfirst ( strtolower($rtype ));

try {
    if (class_exists ( $resp )) {
        $resp = new $resp ( );
    } else {
	$resp = new xRendererDefault();
    }
}
catch ( Exception $e ) {
    $resp = new xRendererDefault();
}

echo $resp->out ();
ob_flush();
die ();
?>