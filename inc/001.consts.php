<?php
	define('XENDRI_VERSION', 0.1);
        define('start_time_ms', microtime(true));

        // time consts
        define('SEC_MSEC', 1000);
        define('MIN_MSEC', SEC_MSEC*60);
        define('HOUR_MSEC', MIN_MSEC*60);
        define('DAY_MSEC', HOUR_MSEC*24);
        define('WEEK_MSEC', DAY_MSEC*7);
        define('YEAR_MSEC', DAY_MSEC*365);
        define('HYEAR_MSEC', DAY_MSEC*366);

        define('HOUR_SEC', 60*60);
        define('DAY_SEC', HOUR_SEC*24);
        define('WEEK_SEC', DAY_SEC*7);
        define('YEAR_SEC', DAY_SEC*365);
        define('HYEAR_SEC', DAY_SEC*366);
?>