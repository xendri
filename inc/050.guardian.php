<?php
/*
 * This is a guardian file for Xendri CMS
 * File autoudates his self every week from Xendri server.
 * If you want to disable autoupdating - just change variable $update_interval
 *      to false
 *
 *
 * Этот файл защищает Xendri CMS от наиболее распространённых и известных
 *      разработчикам системы атак.
 * Файл автоматически обновляется с сервера CMS Xendri еженедельно.
 * Для того, чтобы отменить автоматическое обновление, присвойте переменной
 *      $update_interval значение false.
 *
 */

$update_interval = false;// 60*60*24*7;
$update_url = 'http://xendri.ru/etc/cache/guardian-'.XENDRI_DISTR.'-'.XENDRI_BUILD.'.txt';

// autoupdating
if ($update_interval !== false) {
    $file_date = filemtime('etc/cache/guardian.txt');
    if (time() - $file_date > $update_interval || true) {
        copy($update_url, 'etc/cache/guardian.txt');
    }
}

// parameters check

?>
