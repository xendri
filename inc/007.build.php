<?php
	$build = explode('.', X::cfg('log')->xendri_build);
	if (strpos($build[0], 'd') > -1) {
		if (date("Y-m-d", X::cfg('log')->mtime()) != date("Y-m-d")) {
			$build[1]++;
			X::cfg('log')->xendri_build = implode('.', $build);
		}
		X::cfg('log')->save();
	}
	define('XENDRI_DISTR', $build[0]);
	define('XENDRI_BUILD', $build[1]);
?>