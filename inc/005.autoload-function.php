<?php
#<set_include_path#
set_include_path(
        #<usr_include_path#
        ''.
        #usr_include_path>#
        #<ext_include_path#
        './usr'.
        PATH_SEPARATOR.'./lib'.
        #ext_include_path>#
        PATH_SEPARATOR.get_include_path()
);
#set_include_path>#
#<__autoload#
function __autoload($class)
{
    #<code#
    if(class_exists($class, false)) {
        return true;
    }

    $camel = explodeCase($class, false);
    if (strlen($camel[0]) == 1) {
        $camel[0] = strtoupper($camel[0]);
    }
    $file = implode($camel, '/').'.php';

    require_once($file);
    if (class_exists($class, false)) {
        return true;
    }

    return false;
    #code>#
}
#__autoload>#
#<explodeCase#
/**
 * Splits up a string into an array similar to the explode() function but according to CamelCase.
 * Uppercase characters are treated as the separator but returned as part of the respective array elements.
 * @author Charl van Niekerk <charlvn@charlvn.za.net>
 * @author Dmitriy O. Chechotkin <d.chechotkin@gmail.com>
 * @param string $string The original string
 * @param bool $lower Should the uppercase characters be converted to lowercase in the resulting array?
 * @return array The given string split up into an array according to the case of the individual characters.
 */
function explodeCase($string, $lower = true)
{
    # Initialise the array to be returned
    $array = array();

    # Initialise a temporary string to hold the current array element before it's pushed onto the end of the array
    $segment = '';

    $low_char_flag = false;
    $first = true;

    # Loop through each character in the string
    foreach (str_split($string) as $char) {
        # If the current character is uppercase
        if ((ctype_upper($char) or ctype_digit($char))
                && $low_char_flag) {
            # If the old segment is not empty (for when the original string starts with an uppercase character)
            if ($segment) {
                # Push the old segment onto the array
                if ($first && $segment == 'x') $segment = 'X';
                $first = false;
                $array[] = $segment;
            }

            # Set the character (either uppercase or lowercase) as the start of the new segment
            $segment = $lower ? strtolower($char) : $char;
            $low_char_flag = false;
            $first = false;
        } else { # If the character is lowercase or special
            # Add the character to the end of the current segment
            $segment .= $char;
            $low_char_flag = ctype_lower($char);
        }
    }

    # If the last segment exists (for when the original string is empty)
    if ($segment) {
        if ($first && $segment == 'x') $segment = 'X';
        # Push it onto the array
        $array[] = $segment;
    }

    # Return the resulting array
    return $array;
}
#explodeCase>#
?>
