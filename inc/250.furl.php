<?php

/**
 * 
 * Friendly URL parser call.
 *
 * Вызов парсера ЧПУ
 * 
 */

    $_SERVER['REQUEST_URI'] = urldecode($_SERVER['REQUEST_URI']);
    $url = substr($_SERVER['REQUEST_URI'], 1);
    if(file_exists(xFsTools::enc_name($url))) {
        $mime = FSTools::mime($url);
        header('Content-type: '+$mime+'; charset=windows-1251');
        header('Content-disposition: inline');
        FSTools::printFile($url);
        die('');
    }
    $url = explode('/', $url);
    if (count($url) > 1) {
        $className = 'Furl'.addslashes(ucfirst(strtolower($url[0])));
    } else {
        $className = 'FurlDefault';
    }
    try {
        class_exists($className);
    } catch (Exception $e) {
        $className = 'FurlDefault';
    }
    $params = null;
    eval('$params = '.$className.'::parse($_SERVER["REQUEST_URI"]);');
    $_GET = array_merge($_GET, $params);
?>