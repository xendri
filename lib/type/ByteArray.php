<?php
class ByteArray implements ArrayAccess, Countable {
    protected $data = array();

    protected function __construct(array $data) {

    }

    function offsetExists($offset) {
        return isset($this->data[$offset]);
    }

    function offsetGet($offset) {
        return $this->data[$offset];
    }

    function offsetSet($offset, $value) {
        $this->data[$offset] = $value;
    }

    public function offsetUnset($offset) {
        unset($this->data[$offset]);
        $this->data = array_values($this->data);
    }
    
    public function count() {
        return count($data);
    }
}
?>