<?php

interface ByteArrayConvertable {
	/**
	 * @return array
	 */
	public function toBytesArray();
	
	public function toByteArray();
	
	public function fromByteArray(ByteArray $array);
}

?>