<?php

/**
 * Description of DatabaseProvider
 *
 * @author user
 */
interface DatabaseProviderInterface {
    static function connect($server = null, $login = null, $password = null, $port = null);
    static function initialize();
    static function database($database);
    static function query($query, $params = null);
    static function error();
    static function insertion_id();
    static function affected_rows();
}
?>
