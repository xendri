<?php

/**
 *
 * @author d.chechotkin
 */
interface SelectQueryInterface {
    

    public function __constrict(string $table);

    public function columns(string $list);
    public function from(string $table);
    public function where(string $where, mixed $params);
    public function join(string $table, string $on, mixed $params);
    public function left_join(string $table, string $on, mixed $params);
    public function right_join(string $table, string $on, mixed $params);
    public function cross_join(string $table, string $on, mixed $params);
    public function order(string $by);
    public function limit(integer $count);
    public function part(integer $from, integer $count = null);

    public function assembly();
}
?>
