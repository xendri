<?php
interface XMLConvertable
{

	/**
	 * @param $parent
	 * @return SimpleXMLElement
	 */
	function toXML(SimpleXMLElement &$parent = NULL);
}
?>