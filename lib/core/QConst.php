<?

/**
 * Q constants list
 *
 */
class QConst
{
	/**
	 * Requested class not founded exception
	 *
	 */
	const X_CLASS_NOT_FOUND = 404000;
	/**
	 * Requested configuration can not be found
	 *
	 */
	const X_CONFIG_NOT_FOUND = 404001;
	/**
	 * Requested file not founded
	 *
	 */
	const X_FILE_NOT_FOUND = 404002;
	/**
	 * Requested dir not found
	 */
	const X_DIR_NOT_FOUND = 404003;
	/**
	 * Application not found
	 */
	const X_APP_NOT_FOUND = 404004;
	/**
	 * Application state not found
	 */
	const X_STATE_NOT_FOUND = 404005;

	/**
	 * Database connection failed
	 */
	const X_DB_FAULT = 399999;

	/**
	 * User not admin
	 */
	const X_USER_NOT_ADMIN = 401001;

	/**
	 * RQL query invalid exception
	 *
	 */
	const X_BAD_RQL = 400000;

	/**
	 * RQL query bad alternation exception
	 *
	 */
	const X_BAD_ALTERNATION = 400001;
	const X_BAD_REQUEST = 400002;

	const RQL_SELECT = 1;
	const RQL_UPDATE = 2;
	const RQL_DELETE = 4;
	const RQL_CREATE = 16;

	const ACL_CHILD_OPERATION = 32;
	const ACL_RELATION_OPERATION = 64;

	/**
	 * Configuration file not writable
	 */
	const X_CONFIG_NOT_WRITABLE = 500000;

	/**
	 * Request class: Unable to start simulation
	 */
	const X_SIMULATION_FAILED = 500001;

	/**
	 * Undefined exception
	 */
	const X_UNDEFINED = 999999;
}

?>