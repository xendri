<?php

class PageElement {
	private $query;
	public $handler;
	private $content;
	
	function __construct(SimpleXMLElement $content) {
		$this->content = $content;
		$this->handler = $content->getName ();
		$this->query = $content ['source'];
	}
	
	function getValue() {
		$handler = 'pe_' . strtolower ( $this->handler );
		try {
			class_exists ( $handler );
		} catch ( Exception $e ) {
			return $this->content->asXML ();
		}
		$handler = new $handler ( $this->content );
		$rc = $handler->getContents ();
		if (!$rc === null) {
			if (( string ) $this->content)
				$rc = ( string ) $this->content;
			else
				$rc = 'No result returned.';
		} 
		return '<' . $this->handler . ' id="' . $this->content ['id'] . '">' . $rc . '</' . $this->handler . '>';
	}

}

?>