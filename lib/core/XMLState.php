<?php
	class XMLState implements XMLConvertable {
		protected $file;
		
		function __construct($app, $state) {
			$this->file = Styler::locate($app.'.'.$state.'.xslt');
		}
		
		function toXML(SimpleXMLElement &$parent = NULL) {
			$src = '<?xml version="1.0" encoding="UTF-8"?>'.
					'<?xml-stylesheet type="text/xsl" href="'.$this->file.'"?>'.
					'<root/>';
			$xml = simplexml_load_string($src);
			return $xml;
		}
	}
?>