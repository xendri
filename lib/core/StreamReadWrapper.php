<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Admin
 */
interface StreamReadWrapper {
    
    public function stream_open($path, $mode, $options, &$opened_path);
    public function stream_read($count);
    public function stream_tell();
    public function stream_eof();
    public function stream_seek($offset, $whence);
    public function stream_stat();

}
?>
