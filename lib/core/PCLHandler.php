<?php

class PCLHandler {
    private $source;
    private $name = '';
    function __construct($name) {
        $this->name = $name;
        $file = $name.".pcl";
        if (is_a($file, 'SimpleXMLElement')) {
            $file = $file['src'];
        }
        try {
            $this->source = Styler::locate ( $file );
        }
        catch (Exception $e) {
            return false;
        }
        $res = '';
        if (! trim ( $this->source )) {
            Log::warning ( 'Unable to find page contents list!' );
        }
    }
    function getContents() {
        if (!$this->source) return false;
        $pcl = simplexml_load_file ( $this->source );
        if (!$pcl) Log::fatal('Bad pcl file '.$this->source, QConst::X_UNDEFINED);
        $ret = $this->recurseXML($pcl);
        return $ret;
    }

    function recurseXML(SimpleXMLElement $node) {
        $childs = $node->children();
        $res = '';
        if (count($childs) > 0) {
            $res = '<'.$node->getName().'>';
            foreach ( $childs as $content ) {
                $res .= $this->recurseXML($content);
            }
            $res .= '</'.$node->getName().'>';
        }
        else {
            $pe = new PageElement ( $node );
            $res .= $pe->getValue ();
        }
        return $res;
    }

}

?>