<?php

interface pclHandlerInterface {
	function __construct(SimpleXMLElement $contents);
	function getContents();
}

?>