<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Guardian {
    static function check_file(&$file) {
        if (substr($file, 0, 1) === '/') {
            log::fatal('Invalid file name: '.$file);
        }
        if (strpos($file, '..')>-1) {
            log::fatal('Invalid file name: '.$file);
        }
    }

    public static function check_regexp_safety(string $string) {
        if (strpos($string, chr(0)) !== false) Log::fatal("Invalid regexp: zero character is disallowed in regexps!");
        if (strpos($string, '\0') !== false) Log::fatal("Invalid regexp: zero character is disallowed in regexps!");
    }

    public static function sql(string &$string) {
        $string = mysql_real_escape_string($string);
    }

    public static function shellarg(string &$string) {
        $string = escapeshellarg($string);
    }

    public static function shellcmd(string &$string) {
        $string = escapeshellcmd($string);
    }

    public static function check(string $string) {
        self::check_regexp_safety($string);
    }

    public static function is_int($data) {
        if (intval($data).'' == $data) {
            return true;
        } else return false;
    }

    public static function is_db_fieldname(string $name) {
        return DatabaseProvider::is_name($name);
    }
}

?>
