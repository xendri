<?php
	class XMLPage implements XMLConvertable {
		protected $pagename;
		
		function __construct($pgn) {
			$this->pagename = $pgn;
			if (!file_exists('usr/xml/pages/'.$this->pagename.'.xml')) {
				Log::fatal('XMLPage `'.$pgn.'` not found!', QConst::X_FILE_NOT_FOUND);
			}
		}
		
		function toXML(SimpleXMLElement &$parent = NULL) {
			$src = file_get_contents('usr/xml/pages/'.$this->pagename.'.xml');
			if (!$parent) $xml = simplexml_load_string($src);
			else {
				$xml = $parent->addChild('page');
			}
			foreach ($this->elements as $key=>$elem)
			{
				if (is_a($elem, 'XMLConvertable')) {
					$elem->toXML($xml);
				} else $xml->$key = $elem.'';
			}
			return $xml;
		}
	}
?>