<?php
	class Response implements XMLConvertable, JSONConvertable
	{
		protected $result;
		
		public function __set($name, $val)
		{
			$this->result[$name] = $val;
		}
		
		public function __get($name)
		{
			return $this->result[$name];
		}
		
		public function toXML(SimpleXMLElement &$parent = NULL)
		{
			$def = $this->result;
			if ($parent === NULL) $xml = simplexml_load_string('<response/>');
			else {
				$xml = $parent->addChild('response');
			}
			foreach($def as $field => $val)
			{
				$xml->addChild($field, $val);
			}
			return $xml;
		}
		
		public function toJSON(){
			return json_encode($this->result);
		}
	}
?>