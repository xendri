<?
	class htaccess 
	{
		private $order;
		private $deny;
		private $allow;
		private $code;
		
		function __construct($dir)
		{
			if (!is_dir($dir)) throw new XMLException(QConst::X_DIR_NOT_FOUND, "'$dir' isn't a directory!");
			if (file_exists($dir.'/.htaccess')) $htcode = file_get_contents($dir.'/.htaccess');
			$this->code = $htcode;
			$this->order = $this->get_order($htcode);
		}
		
		private function get_order($code)
		{
			$regexp = '/order\\s(deny|allow),\\s?(deny|allow)/i';
			preg_match($regexp, $code, $matches);
			array_shift($matches);
			return $matches;
		}
		
		private function get_acces($code)
		{
			$regexp = '/(deny|allow)\\s+from\\s+(all|[^\\s]+)/i';
			preg_match_all($regexp, $code, $mathes);
			return $mathes;
		}
	}
?>