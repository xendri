<?php
interface JSONConvertable
{

	/**
	 *
	 * @return String
	 */
	function toJSON();
}
?>