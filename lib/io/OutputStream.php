<?php
interface OutputStream {
    public function __construct(string $file);
    public function rewrite();
    public function append();
    public function write($data);
    public function writeln(string $data);
    public function write_byte(byte $data);
    public function flush();
    public function close();
    public function is_opened();
}
?>