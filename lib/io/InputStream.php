<?php
	interface InputStream {
		public function __construct(string $fileName);
		public function open();
		public function is_opened();
		public function is_ended();
		public function close();
		public function read(integer $size = NULL);
		public function read_line();
		public function available();
	}
?>