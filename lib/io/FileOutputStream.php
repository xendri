<?php

lib('io.OutputStream');

class FileOutputStream implements OutputStream {

    protected $target;

    public function __construct(string $file) {
        $this->target = $file;
    }

    public function rewrite() {
        $this->target = fopen($this->target, "w");
    }

    public function append() {
        $this->target = fopen($this->target, "a");
    }

    public function write($data) {
        fwrite($this->target, $data);
    }
    public function flush() {
        fflush($this->target);
    }
    public function close() {
        fclose($this->target);
    }
    public function writeln(string $data) {
        $this->write($data."\r\n");
    }
    public function write_byte(byte $data) {
        $this->write($data);
    }

    public function is_opened() {
        return is_resource($this->target);
    }

}
?>