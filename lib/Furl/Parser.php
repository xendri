<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

interface FurlParser {

    /**
     * This function gets an Friendly URL and return an array of parameters
     *
     * Функция принимает человекопонятный урл и возвращает массив параметров, указанный в урле.
     * 
     * @param string $furl
     */
    public static function parse(string $furl);

}

?>
