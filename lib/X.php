<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Xendri shortcuts
 *
 * @author d.chechotkin <d.chechotkin@gmail.com>
 */
class X {

    protected static $configs;

    /**
     * @var xLog
     */
    protected static $logs;

    static function sess($name) {
	return @$_SESSION[$name];
    }
    static function sess_set($name, $val) {
	$_SESSION[$name] = $val;
	return $val;
    }

    protected static function make_log($target = null) {
	if ($target === null) $target = 'system';
	if (isset(self::$logs[$target])) return;
	self::$logs[$target] = new xLog();
	self::$logs[$target]->target($target);
    }

    static function notice(string $message, $target = null){
	if ($target === null) $target = 'system';
	self::make_log($target);
	self::$logs[$target]->put($message);
    }

    static function warning(string $message, $target = null) {
	if ($target === null) $target = 'system';
	self::make_log($target);
	self::$logs[$target]->warning($message);
    }

    static function fatal(string $message, $code = null) {
	$target = 'fatal';
	self::make_log($target);
	self::$logs[$target]->fatal($message, $code);
    }

    public static function cfg($name) {
	if (strpos($name, ':') !== false) {
	    list($src, $var) = explode(':', $name);
	    $res = self::cfg($src)->$var;
	    return $res;
	}
        if (!isset(self::$configs[$name])) {
            self::$configs[$name] = new xConfig($name);
        }
        $ret = &self::$configs[$name];
	return $ret;
    }

    public static function cfg_set($name, $val) {
	list($src, $var) = explode(':', $name);
        if (!isset(self::$configs[$src])) {
            self::$configs[$src] = new xConfig($name);
        }
	self::$configs[$src]->$var = $val;
	return true;
    }

    public static function param($name) {
	return xRequest::param($name);
    }

    public static function params() {
	return xRequest::params();
    }

    static function locate($file) {
	return xStyler::locate($file);
    }

    static function user() {
	return xAuth::user();
    }

    static function userid() {
	return xAuth::user()->id;
    }

}
?>
