<?

class MixedRecord extends Record implements XMLConvertable  {

	private $types;

	function __construct($sql, $types) {
		if (!is_array($types) && @strstr($types, ',')) $types = explode(',', $types);
		elseif(!is_array($types)) $types = array($types);
			
		$afields = mysqli_fetch_array($sql, MYSQLI_NUM);

		if (!is_array($afields)) {
			throw new Exception('no data in SQLResult!', 1011);
		}
		foreach ($types as $tx => $type) {
			if (!$type) continue;
			$type = new RecordType(trim($type));
			$defs[$tx] = $type->get_def();
			foreach ($defs[$tx] as $fx => $field_name) {
				$fields[$field_name] = @$afields[($tx*(count($defs[$tx-1])+1))+$fx+1];
			}
			$fields['acl'] = @$afields[($tx*(count($defs[$tx-1])+1))];
			$this->elements[$type->get_name()] = new Record($fields, $type->get_name());
		}
		$this->types = $types;
	}

	/**
	 * Returns record of specified RecordTypeName
	 *
	 * @param unknown_type $name
	 * @return Record
	 */
	function __get($name) {
		return @$this->elements[$name];
	}

	function __set($name, $val) {
		throw new Exception('unable to set property of MixedRecord!!!');
	}

	function get_types() {
		return $this->types;
	}

	function toXML(SimpleXMLElement &$parent = NULL) {
		$def = $this->elements;
                $count = count($def);
		if ($parent === NULL) $xml = simplexml_load_string('<mixedrecord/>');
		else {
			$xml = $parent->addChild('mixedrecord');
			$xml['types'] = implode(',', $this->types);
		}
                unset($def['###']);
		foreach($def as $field => $val)
		{
			$val->toXML($xml);
		}
		return $xml;
	}
}