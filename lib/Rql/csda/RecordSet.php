<?php


class RecordSet extends Set implements XMLConvertable  {

	private $types;

	function __construct($sqlResponse, $typesMix, $count) {
	    $rows = array();
		if ($count == 0) Log::fatal('no rows affected!', 1010);
		try {
		    for ($x=0;$x<$count;$x++) {
			$row = new MixedRecord($sqlResponse, $typesMix);
			$rows[] = $row;
		    }
		} catch(Exception $e) {
		    if (count($rows) == 0) throw $e;
		}
		$this->types = (is_array($typesMix)) ? $typesMix : array($typesMix);
		parent::__construct($rows);
	}


	function __set($name, $val) {
			
	}

	function __get($name) {
			
	}

	/**
	 * Возвращает запись с указанным номером
	 *
	 * @param numeric $number
	 * @return Record
	 */
	function item($number) {
		return $this->elements[$number];
	}

	function add(Record $rec) {
		if (!is_a($rec, 'Record')) throw new Exception('Cannot add class `'.get_class($rec).'` to RecordSet.');
		if ($rec->getDef() != $this->def) throw new Exception('Cannot add this type of record to RecordSet: invalid definition.');
		array_push($this->elements, $rec);
	}

	function toXML(SimpleXMLElement &$parent = NULL)
	{
		if (!$parent) $xml = simplexml_load_string('<recordset count="'.$this->count().'" name="'.$this->name.'"/>');
		else {
			$xml = $parent->addChild('recordset');
			$xml['name'] = $this->name;
		}
		foreach ($this->elements as $key=>$elem)
		{
			$elem->toXML($xml);
		}
		return $xml;
	}

	function get_types() {
		return $this->types;
	}
}

?>