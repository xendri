<?

class RecordType {

	private $def;
	private $name;

	function __construct($name) {
		$this->name = $name;
                if (class_exists('Config')) {
                    $cfg = new Config('sql');
                    $flag = $cfg->cache_types;
                } else {
                    $flag = false;
                }
		if (!@$_SESSION['rtcache'][$name] || !$flag) {
                    $conn = RQL::get_connection();
			$def = mysqli_query($conn, 'SHOW COLUMNS FROM `'.$name.'`');
                        if (!$def) throw new Exception(mysqli_error($conn));
			while ($field = mysqli_fetch_array($def, MYSQL_ASSOC)) {
				$res[] = $field['Field'];
			}
			if ($flag) {
                            $_SESSION['rtcache'][$name] = $res;
                        } else {
                            $this->def = $res;
                            return;
                        }
		}
		$this->def = $_SESSION['rtcache'][$name];
	}

	function get_def() {
		return $this->def;
	}

	function get_name() {
		return $this->name;
	}
}