<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of RqlDescription
 *
 * @author Admin
 */
class RqlDescription {
    public $fields = array();

    public $handlers = array();

    protected $opened_element;

    const READING						    = 2;
    const WAITING_CLOSING_ELEMENT    				    = 4;
    const IGNORING_SYMBOL					    = 8;

    protected $state;
    protected $target;
    protected $element;
    protected $buffer;

    public function  __construct($description) {
	// initing $handlers
	$this->handlers[self::READING]['actions']['"'] = 'opening_element';
	$this->handlers[self::READING]['actions']["'"] = 'opening_element';
	$this->handlers[self::READING]['actions']['='] = 'target_switcher';
	$this->handlers[self::READING]['actions'][','] = 'target_switcher';
	$this->handlers[self::READING]['default'] = 'char_reader';

	$this->handlers[self::WAITING_CLOSING_ELEMENT]['actions']['"'] = 'opening_element';
	$this->handlers[self::WAITING_CLOSING_ELEMENT]['actions']["'"] = 'opening_element';
	$this->handlers[self::WAITING_CLOSING_ELEMENT]['actions']["\\"] = 'slash_handler';
	$this->handlers[self::WAITING_CLOSING_ELEMENT]['default'] = 'char_reader';

	$this->handlers[self::IGNORING_SYMBOL]['default'] = 'char_reader';

	$this->start(str_split($description));
    }

    protected function start($characters) {
	while($char = array_shift($characters)) {
	    if (!key_exists($this->state, $this->handlers)) {
		throw new Exception('RqlDescription internal error: state `'.$this->state.'` is invalid');
	    }
	    if (key_exists($char, $this->handlers[$this->state]['actions'])) {
		$handler = $this->handlers[$this->state]['actions'][$char];
	    } else {
		$handler = $this->handlers[$this->state]['default'];
	    }
	    if (method_exists($this, $handler)) {
		throw new Exception('RqlDescription internal error: handler '.$handler.'` is invalid');
	    }
	    $this->$handler($char);
	}
	return true;
    }

    protected function char_reader($char) {

    }
}
?>
