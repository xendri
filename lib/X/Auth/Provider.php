<?php
/**
 *
 * @author d.chechotkin
 */
interface xAuthProvider {

    const AUTH_GUEST        = 401;
    const AUTH_CONTINUED    = 100;
    const AUTH_OK           = 200;
    const AUTH_FORBIDDEN    = 403;
    const AUTH_FAILED       = 404;

    public function continue_session();
    public function pass($login, $password = null);
    public function guest($user_hash);
    public function user();


}

?>
