<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of XDAL
 *
 * @author Admin
 */
class xAuthProviderXDAL implements xAuthProvider {
    protected static $state;
    protected static $user;

    //put your code here
    public static function init() {
	new Rql();
    }

    public function continue_session() {
        $user = X::sess('user');
        if (!$user) return false;

        $user = Rql::exec('self / user / group / $user[id='.$user.'] / $auth');
	if ($user === false) return false;
        if ($user->count() == 0) {
            unset($_SESSION['user']);
            return false;
        }
        $mix = $user->current();
        if (!$mix->user->reg) {
            self::$state = xAuthProvider::AUTH_FAILED;
            return false;
        } else {
            Rql::set_self($mix->user);
            self::$state = xAuthProvider::AUTH_CONTINUED;
	}
        X::notice('session restored');
        return true;
    }
    public function guest($user_hash) {
	$user_hash = md5($user_hash);
	echo $user_hash."<hr/>\r\n\r\n";
	$user = Rql::exec('self/group/$user/auth[login="'.$user_hash.'"]', null, '1');
	if (!$user) {
	    echo "adding guest...<br/>\r\n";
	    $user = Rql::exec('self/+$user/+auth[set @login="'.$user_hash.'"]', null, '1');
	}
	var_dump($user->current()); die();
	Rql::set_self($user->current(), true);
	return $user;
    }
    public function pass($login, $password = null) {
	if ($password === null) X::fatal('password cannot be empty!');
	$user = Rql::exec('self/group/$user/auth[login=md5("'.$login.'"),password=md5("'.$password.'")]', null,  '1');
	if (!$user) {
	    self::$state = xAuthProvider::AUTH_FAILED;
	    return false;
	}
	$user = $user->current()->user;

	self::$state = xAuthProvider::AUTH_OK;
	$_SESSION['user'] = $user->id;
	self::$user = $user;
	Rql::set_self($user);
    }
    public function user() {
	return self::$user;
    }
}
?>
