<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of XAPPWrapper
 *
 * @author Admin
 */
class xFsWrapperXAPP implements StreamReadWrapper {

    protected $res;

    public function stream_open($path, $mode, $options, &$opened_path) {
        $q = parse_url($path);
        parse_str(@$q['query'], $params);
        $state = explode('/', $q['path']);
        $state = $state[1];
        Log::put('xapp path: '.$path, 'system.log');
        $app = new Application($q['host']);
        $this->res = $app->state($state, $params);
        if (!(is_a($this->res, 'XMLConvertable'))) {
                Log::fatal('Application state must return object with XMLConvertable interface!.');
        }
        $this->res = $this->res->toXML()->asXML();
        return true;
    }
    public function stream_seek($offset, $whence) {
    }
    public function stream_eof() {
    }
    public function stream_tell() {
    }
    public function stream_stat() {
        return array_fill(0, 12, 0);
    }
    public function url_stat() {
        return $this->stream_stat();
    }
    public function stream_read($count) {
        $rt = str_split($this->res, $count);
        $ret = array_shift($rt);
        $this->res = implode('', $rt);
        return $ret;
    }

}


?>
