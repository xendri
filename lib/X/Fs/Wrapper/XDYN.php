<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of XDYNWrapper
 *
 * @author Admin
 */
class xFsWrapperXDYN implements StreamReadWrapper {

    protected $res;

    public function stream_open($path, $mode, $options, &$opened_path) {
        $q = parse_url($path);
        parse_str(@$q['query'], $params);
        $file = Styler::locate($q['host']);
        $this->res = new TextReturn("No XDYN `$path` found");
        $return = NULL;
        if (trim($file) != false) {
            $code = file_get_contents($file);
            eval('?>'.$code.'<?');
            if (!$return) {
                    Log::fatal('XDYN element `'.$q['host'].'` located at <'.$file.'> must return object with XMLConvertable interface!.');
            }
        } else {
            Log::fatal('XDYN element `'.$q['host'].'` is not found');
        }
        if ($return != NULL) $this->res = $return;
        if (is_a($return, 'XMLConvertable')) $this->res = $this->res->toXML()->asXML();
        else $this->res = $return;
        return true;
    }
    public function stream_seek($offset, $whence) {
    }
    public function stream_eof() {
    }
    public function stream_tell() {
    }
    public function stream_stat() {
        return array_fill(0, 12, 0);
    }
    public function url_stat() {
        return $this->stream_stat();
    }
    public function stream_read($count) {
        $rt = str_split($this->res, $count);
        $ret = array_shift($rt);
        $this->res = implode('', $rt);
        return $ret;
    }

}

?>
