<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of XDYNWrapper
 *
 * @author Admin
 */
class xFsWrapperSKIN implements StreamReadWrapper {

    protected $res;
    protected $fp;

    public function stream_open($path, $mode, $options, &$opened_path) {
        $q = parse_url($path);
        parse_str(@$q['query'], $params);
        $file = Styler::locate($q['host']);
        $this->fp = fopen($file, "r");
        $this->res = $file;
        return true;
    }

    public function stream_seek($offset, $whence) {
    }

    public function stream_eof() {
        return feof($this->fp);
    }

    public function stream_tell() {
    }

    public function stream_stat() {
        return fstat($this->fp);
    }
    public function url_stat($url) {
        return array_fill(0, 12, 0);
    }
    public function stream_read($count) {
        return fread($this->fp, $count);
    }

    public function stream_close() {
        return fclose($this->fp);
    }

}


?>
