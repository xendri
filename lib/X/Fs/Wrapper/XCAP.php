<?php
/**
 * Description of CSDAWrapper
 *
 * @author chedim
 */
class xFsWrapperXCAP implements StreamReadWrapper {

    private $xmlData;
    private $ext;

    public function stream_read($count) {
        $rt = str_split($this->xmlData, $count);
        $ret = array_shift($rt);
        $this->xmlData = implode('', $rt);
        return $ret;
    }

    public function stream_tell() {
    }

    public function stream_open($path, $mode, $options, &$opened_path) {
        $ext = '.xml';
        if (strpos($path, '~') > -1) {
            $path = array_pop(explode('~', $path));
        }
        $q = parse_url($path);
        if (strpos($q['path'], '.') > -1) {
            $ext = '.'.aray_pop(explode('.', $q['path']));
        }
        $path = $q['path'];
        $path = str_replace('/', ', ', str_replace('.'.$ext, '', $path)).'.';
        $this->ext = $ext;
        $filter = @$q['query'];
        parse_str(@$q['fragment'], $params);
        try {
            $recs = DataSource::select($path, @$filter, @$params['order'], @$params['limit'], @$params['group']);
            if ($ext != '.xml') {
                $this->xmlData = $this->transform($recs, $ext, 'xml');
            } else $this->xmlData = $recs->toXML()->asXML();
        }
        catch (XMLException $e) {
            if ($e->getCode() != 999999) {
                return false;
            }
            $this->xmlData = '<recordset count="0"/>';
        }
        return true;
    }
    public function stream_seek($offset, $whence) {
    }
    public function stream_eof() {
        return strlen($this->xmlData) > 0;
    }
    public function stream_stat() {
        return array_fill(0, 12, 0);
    }

    public function url_stat() {
        return $this->stream_stat();
    }

    protected function transform(MixedRecord $rec, string $ext, string $ct) {
        $file = Styler::locate ( implode ( '.', $rec->get_types () ) . '-' . $ct . $ext );
        if (! $file)
            $file = Styler::locate ( implode ( '.', $rec->get_types () ) . $ext );

        if (! $file) {
            Log::warning ( $ext . ' file for type ' . implode ( '.', $rec->get_types () ) . ' not found...' );
            if (strtolower ( $ct ) == 'xml') {
                return $rec->toXML ()->asXML ();
            }
            if (strtolower ( $ct ) == 'json') {
                return $rec->toJSON ();
            }
            Log::fatal ( 'View file "' . implode ( '.', $rec->get_types () ) . $ext . '" not found!', QConst::X_FILE_NOT_FOUND );
        }
        else {
            Log::put ( $ext . ' file for type ' . implode ( '.', $rec->get_types () ) . ' found at ' . $file );
        }

        $xslDoc = new DOMDocument ( );
        $xslDoc->load ( $file );

        $xmlDoc = new DOMDocument ( );
        $xmlDoc->loadXML ( $rec->toXML ()->asXML () );

        $test = 'test string';

        $proc = new XSLTProcessor ( );
        $proc->importStylesheet ( $xslDoc );
        return $proc->transformToXML ( $xmlDoc );
    }
}


?>
