<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
*/

class xLibXSLT {

    private static $vars;

    static function set($name, $value, $shortcut = NULL) {
        self::$vars[$name] = array(
                'shortcut' => $shortcut,
                'value' => $value
        );
    }

    static function transform($xml, $xsl) {
        $xml = self::get_DOM($xml);
        $xsl = self::get_DOM($xsl);
        return self::transformDOM($xml, $xsl);
    }

    static function transformXML(string $xml, string $xslt_file) {
        return self::transformDOM(self::to_DOM($xml), $xslt_file);
    }

    static function transformFile(string $file, string $xslt_file) {
        return self::transformDOM(self::load_DOM($xml), self::load_DOM($xslt_file));
    }

    static function transformFileXSL(string $file, string $xsl) {

    }

    static function transformDOM(DOMDocument $xml, DOMDocument $xslDoc) {

        $proc = new XSLTProcessor ( );
        $proc->importStylesheet ( $xslDoc );

        self::init_variables();
        if (is_array(self::$vars)) {
            foreach (self::$vars as $name=>$data) {
                if ($data['shortcut'] != null) {
                    $proc->setParameter('', $data['shortcut'], $data['value']);
                }
                $proc->setParameter('', $name, $data['value']);
            }
        }
        

        $res = $proc->transformToXML ( $xml );

        return $res;
    }

    /**
     * loads XML from file
     * загружает XML из файла
     * @return DOMDocument
     * @param string $file
     */
    private static function load_DOM(string $file) {
        $xml = new DOMDocument ( );
        Guardian::check_file($file);
        if(!@$xml->loadXML ( $file ))
            Log::fatal("Unable to load XML from `$file`!");

        return $xml;
    }

    /**
     *
     * @param string $code
     * @return DOMDocument
     */
    private static function to_DOM(string $code) {
        $xml = new DOMDocument ( );
        Guardian::check_file($file);
        if(!@$xml->load( $code ))
            X::fatal("Unable to load XML from given code!");

        return $xml;
    }

    /**
     *
     * @param mixed $variable
     * @return DOMDocument
     */
    private static function get_DOM($variable) {
	echo '----';
	if(is_a($variable, 'XMLConvertable')) {
            $ret = self::to_DOM($variable->toXML());
        } else {
            try {
                $ret = @self::to_DOM($variable);
            } catch(Exception $e) {
                $ret = @self::load_DOM($variable);
                if (!$ret) {
                    X::fatal("Unable to load DOM from $variable");
                }
            }
        }
        return $ret;
    }

    public static function init_variables() {
        self::set('xendri.session.userid', X::userid(), 's.uid');
//        self::set('xendri.session.userid', str_replace('/', ' ', $_SERVER['QUERY_STRING']), 's.uid');
        $qcu = trim(str_replace('/', ' ', dirname($_SERVER['REQUEST_URI'])).' '.xFsTools::filename($_SERVER['REQUEST_URI']));
        self::set('xendri.query.cssurl', $qcu, 'q.cu');
    }

}

?>
