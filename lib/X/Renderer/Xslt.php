<?php

class XSLTResponse {
    function __construct() {
        header('Content-type: application/xml; charset: utf-8');
    }

    function out() {
        if (Request::param('file')) {
            $file = Request::param('file').'.xslt';
            $file = Styler::locate($file);
        } elseif (Request::param('page')) {
            $xdyn = Request::param('xdyn');
            if (!$xdyn) $xdyn = 'main';
            $pg = Request::param('page');
            if (!$pg) $pg = 'index';
            $file = "$xdyn.$pg.xslt";
            Log::put("requested: $file");
            $file = Styler::locate($file);
        } else {
            $app = Request::param('application');
            $state = Request::param('state');

            $file = 'usr/app/xendri/'.str_replace('.', '_', $app).'/'.str_replace('.','_',$state).'.xslt';
        }

        Log::put('XSLT file: '.$file);
        if (file_exists($file)) {
            echo file_get_contents($file);
        } else {
            echo file_get_contents('usr/xslt/nofile.xslt');
        }
    }
}
?>