<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class XDYNResponse {
    
    function  out() {
        $requested = Request::param ( 'page' );
        if (!$requested) $requested = 'main.index';
        if (!strpos($requested, '.')) $requested = 'main.'.$requested;
        
        $source = array_shift(explode('.', $requested));
        if (!$source) $source = 'main';

        $data = file_get_contents("xdyn://{$requested}.php");
        
        if (!$data) {
            header('404 Not Found');
            die($requested.'.php');
        }

        return $data;
    }
    
}

?>
