<?php
class SUBResponse
{
	function out()
	{
		$class = Request::param('application');
		$method = Request::param('state');
		$xf = $class.'.'.$method.'.xslt';
		

		Log::put('Subresponse from: '.$class.'::'.$method);
		if (!$class)
		{
			$class = 'index';
		}
		if (!$method)
		{
			$method = 'index';
		}
		
		try {
			$class = new Application($class);
			$retVal = $class->state($method);
			
			Log::put('Searching xslt: '.$xf);
			$xf = Styler::locate($xf);
			Log::put('XSLT found at '.$xf);
		} catch (XMLException $e) {
			$xf = Styler::locate('exception.xslt');
			$retVal = $e;
		}
		
		$dom = new DOMDocument();
		$xsl = new XSLTProcessor();
		$dom->load($xf);
		$xsl->importStylesheet($dom);
		
		$dom->loadXML($retVal->toXML());
		
		Log::put('transforming subresult...');
		
		$ret = $xsl->transformToXml($dom);
		return $ret;
	}
}
?>