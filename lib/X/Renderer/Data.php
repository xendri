<?php
	class DATAResponse {
		private $file = '';
		private $ctypes = array(
			'xml' => 'text/xml',
			'json' => 'text/javascript',
			'html' => 'text/html'
		);
		private $ctype;
		private $res;
		private $rt;

		function __construct() {
			$this->ctype = strtolower(Request::param('format')) | 'html';
			header('Content-type: '.$this->ctypes[$this->ctype].'; charset=utf-8');
			flush();
			
			$requested = Request::param('datasource_query');
			$reqRec = $this->getSource($requested); 
			$this->rt = implode('.', $reqRec->get_types());
			$pcl = Styler::locate(implode('.', $reqRec->get_types()).'-'.$this->ctype.'.pcl') | Styler::locate('index-'.$this->ctype.'.pcl') | Styler::locate('index.pcl');
			if (!$pcl) Log::fatal('Unable to find page contents lisrt!', QConst::X_FILE_NOT_FOUND);
			
			$pcl = simplexml_load_file($pcl);
			$childs = $pcl->children();
			$res = '';
			foreach($childs as $content) {
				$query = $content['source'];
				if (strtolower($query) != 'request') {
					$rec = $this->getSource($query);
				} else {
					$rec = $reqRec;
				}
				$ret = $this->transform($rec, 'xml');
				if (!strstr($ret, '<?xml version="1.0"?>')) $ret = '<![CDATA['.$ret.']]>';
				else $ret = trim(str_replace('<?xml version="1.0"?>', '', $ret));
				$res .= '<content id="'.$content['id'].'">'.$ret.'</content>';
			}
			$this->res = '<page>'.$res.'</page>';
		}
		
		function getSource($query) 
		{
			
			if ($query == '') 
			{
				 Log::fatal('Empty request!', QConst::X_BAD_REQUEST);
			}
			
			$query = explode('~', $query);
			$query[0] = str_replace('/', ',', $query[0]);
			$query[0] = str_replace('\,', '/', $query[0]);
			$query[0] = 'cercea,'.$query[0].'.';
			$rec = DataSource::select($query[0], $query[1], $query[2], $query[3], $query[4]);
			
			return $rec;
		}
		
		function transform($rec, $ct) {
			if ($rec->count() > 1) $ext = '.xlv';
			else $ext = '.xrv';
			$file = Styler::locate(implode('.', $rec->get_types()).'-'.$ct.$ext) | Styler::locate(implode('.', $rec->get_types()).$ext);
			
			if (!$file) {
				Log::warning($ext.' file for type '.implode('.', $rec->get_types()).' not found...');
				if (strtolower($ct) == 'xml'){
					return $rec->toXML()->asXML();
				} 
				if (strtolower($ct) == 'json'){
					return $rec->toJSON();
				} 
				Log::fatal('View file "'.implode('.', $rec->get_types()).$ext.'" not found!', QConst::X_FILE_NOT_FOUND);
			}
			
			$xslDoc = new DOMDocument();
			$xslDoc->load($file);
			
			$xmlDoc = new DOMDocument();
			$xmlDoc->loadXML($rec->toXML()->asXML());
			
			$test = 'test string';
			
			$proc = new XSLTProcessor();
			$proc->importStylesheet($xslDoc);
			return $proc->transformToXML($xmlDoc);
		}
		
		function out() {
			$page = Styler::locate($this->rt.'-'.$this->ctype.'.xpt') | Styler::locate('index-'.$this->ctype.'.xpt') | Styler::locate('index.xpt');
			if (!$page) Log::fatal('Unable to find page template!', QConst::X_FILE_NOT_FOUND);
			
			$xslDoc = new DOMDocument();
			$xslDoc->load($page);
			
			$xmlDoc = new DOMDocument();
			$xmlDoc->loadXML($this->res);
			
			
			$proc = new XSLTProcessor();
			$proc->importStylesheet($xslDoc);
			
			$res = $proc->transformToXML($xmlDoc);
			$res = html_entity_decode($res);
			return $res;
		}
	}
	
?>