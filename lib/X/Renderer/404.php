<?php
class xRenderer404 {
	
	function __construct() {
		header ( 'Content-type: text/html; charset=utf-8' );
		$this->res = '<page></page>';
	}
	
	function out() {
		header('404 Not Found');
		$page = X::locate ( '404.xpt' );
		if (! $page) {
			X::fatal ( 'Unable to find page template `404.xpt`!', QConst::X_FILE_NOT_FOUND );
		}
			
		$xslDoc = new DOMDocument ( );
		$xslDoc->load ( $page );
		
		$xmlDoc = new DOMDocument ( );
		$xmlDoc->loadXML ( $this->res );
		
		$proc = new XSLTProcessor ( );
		$proc->importStylesheet ( $xslDoc );
		
		$res = $proc->transformToXML ( $xmlDoc );
		$res = html_entity_decode ( $res );
		return $res;
	}
}

?>