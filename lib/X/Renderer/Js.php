<?php

class JSResponse
{
	function __construct()
	{
		header('Content-type: text/javascript; charset=utf8');
	}

	function out()
	{
		$file = Request::param('page').'.js';
		$file = Styler::locate($file);
                if ($file == '') {
                    Log::fatal404();
                }
                $code = file_get_contents($file);

                $code = "//<\n".JSMin::minify($code)."\n//>";

                return $code;
	}
}
?>