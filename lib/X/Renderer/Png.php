<?php 
	class PNGResponse {
		function __construct() {
			header('Content-type: image/png');
		}
		
		function out(){
			$file = Styler::locate(Request::param('page').'.png');
			return file_get_contents($file);
		}
	}

?>