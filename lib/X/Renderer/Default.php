<?php
class xRendererDefault {
    function __construct() {
        $mime = $GLOBALS['mime_by_ext'][strtolower(xRequest::type())];
        header('Content-type: '.$mime);
    }

    function out() {
        $xdyn = X::param('xdyn');
        $pg = X::param('page');
        if (!$pg) $pg = 'index';
        $file = X::locate($pg.'.'.strtolower(xRequest::type()));
        if (!$file) {
            $resp = new xRenderer404();
            return $resp->out();
        }
        return file_get_contents($file);
    }
}

?>