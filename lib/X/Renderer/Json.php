<?php 
	
	class JSONResponse extends Response {
		function __construct() {
			header('Content-type: text/json');
		}
		
		function out() {
			$app = Request::param('application');
			$model = Request::param('state');
			$application = new Application($app);
			$response = $application->state($model);
			if (!(is_a($response, 'JSONConvertable'))) {
				Log::warning('State <'.$model.'> of application <'.$app.'> returned object of <'.gettype($response).'> type.');
				$resp = json_encode($response);
			} else {
				$resp = $response->toJSON();
			}
			return $resp;
		}
	}
?>