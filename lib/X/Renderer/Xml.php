<?php 
	
	class XMLResponse extends Response {
		function __construct() {
			header('Content-type: text/xml');
		}
		
		function out() {
			$app = Request::param('application');
			$model = Request::param('state');
			$application = new Application($app);
			$response = $application->state($model);
			if (!(is_a($response, 'XMLConvertable'))) {
				Log::fatal('Application state must return object with XMLConvertable interface!.');
			}
			$resp = $response->toXML();
			return $resp->asXML();
			
		}
	}
?>