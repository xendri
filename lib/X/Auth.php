<?php
/* 
* To change this template, choose Tools | Templates
* and open the template in the editor.
*/

/**
 * Description of Auth
 *
 * @author Admin
 */
class xAuth {

    /**
     * @var xAuthProviderXDAL
     */
    protected static $provider;
    static function user() {
	return self::$provider->user();
    }
//put your code here
    static function init($prov = null) {
	if (!class_exists($prov)) {
	    $prov = X::cfg('auth')->default_auth_provider;
	}
	self::$provider = new $prov();
    }

    public static function continue_session() {
	return self::$provider->continue_session();
    }

    public static function pass($login, $password = null) {
	return self::$provider->pass($login, $password);
    }

    public static function guest($user_hash) {
	return self::$provider->guest($user_hash);
    }

    public static function state() {
	return self::$provider->state;
    }
}




?>
