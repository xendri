<?php
/**
 * Description of Tee
 *
 * @author chedim
 */
class xIoTee extends xIoPipe {
    
    /**
     * @var xIoPipeInterface
     */
    protected $target = array();

    public function  __construct($name, $length = 1, $choke = false, &target = null) {
	$this->name = $name;
	$this->length = $length;
	$this->choke = false;
	if ($target !== null) $this->target = $target;
    }

    public function connect(xIoPipeInterface &$target) {
	$this->target[] = $target;
    }

    public function is_occupied() {
	return false;
    }

    public function put($data) {
	$this->buffer[] = $data;
	if (count($this->buffer) > $this->pipe_length) {
	    $send = array_shift($this->buffer);
	    for($i=0; $i<count($this->target); $i++){
		$this->target[$i]->put($send);
	    }
	}
    }
}
?>
