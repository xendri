<?php
/**
 * Description of Interface
 *
 * @author chedim
 */
interface xIoPipeInterface {

    public function put($data);
    public function connect(xIoPipeInterface $target);
    public function connected(xIoPipeInterface $parent);
    public function get_name($full = false);

    public function is_connected();
    public function is_choke();
    public function is_occupied();
    
}
?>
