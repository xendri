<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Occupied
 *
 * @author chedim
 */
class xIoPipeOccupied extends Exception {
  
    public function  __construct($message = 'Cannot connect to pipe: already connected.', $code = 0, $previous = null) {
	parent::__construct($message, $code, $previous);
    }

}
?>